#ifndef VIDEOWIDGET_H
#define VIDEOWIDGET_H

#include <QWidget>
#include "video_thread.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>
//#ifdef __APPLE__
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui.hpp>

//#endif
#include <constval.h>
#include <tracking_object.h>

using namespace std;

using namespace cv;

class videoWidget : public QWidget
{
    Q_OBJECT
public:
    explicit videoWidget(QWidget *parent = 0);
    Mat frame,core_frame,mask_core_frame;
    void initwidget(video_thread *);
    void setframenum(video_thread *,int);
    void filter();
    void detect();
    void setBackGround(Mat b);
    Mat getCurrentFrame();
    void setSize(int,int);

signals:

public slots:
    void getNewFrame(Mat*);
    void setMask();

protected:
    void paintEvent(QPaintEvent *);

public:
    bool resize_state;
    int video_width,video_height;

    QList<int> filter_seq;
    bool filter_state;
    Mat filter_out,back;
    int canny_lowthresh,canny_highthresh,thresh_type;
    int thresh_low,thresh_high;
    int dilate_val , erode_val;

    double alfa;
    vector<vector<Point> > contours;
    vector<Vec4i> hierarchy;

    Mat count_img,frame_greyImage,diff_image;
    Mat back_greyImage;
    Mat mask;
    int detiction_area;
    bool firstTime;

    vector<Tracking_Object> objects;
   // vector<Tracking_Object> prev_objects;
};

#endif // OUT_WIDGET_H
