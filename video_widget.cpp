#include "video_widget.h"
#include <QPainter>
#include <iostream>
#include <QDebug>

videoWidget::videoWidget(QWidget *parent) : QWidget(parent)
{
    filter_state = false;
    resize_state = false;
    video_height = 0;
    video_width = 0;
    mask = imread("./mask.png",CV_8UC1);
    mask_core_frame.create(mask.size(), mask.type());

    firstTime = true;
}

void videoWidget::initwidget(video_thread *vid)
{
   connect(vid , SIGNAL(commingFrame(Mat*)) , this ,SLOT(getNewFrame(Mat*)));
}

Mat videoWidget::getCurrentFrame()
{
    return frame;
}

void videoWidget::setBackGround(Mat b)
{
    back = b;
}

void videoWidget::setMask()
{
    mask = imread("./mask.png",CV_8UC1);
}

void videoWidget::detect()
{
    int sheeps =0;
    Mat frame_mask,back_mask;
    frame.copyTo(frame_mask,mask);
    core_frame.setTo(0);
    mask_core_frame.setTo(0);

    back.copyTo(back_mask,mask);
    cvtColor(frame_mask, frame_greyImage, CV_BGR2GRAY);
    cvtColor(back_mask, back_greyImage, CV_BGR2GRAY);

    //imshow("diff_image1",frame_greyImage);
    //imshow("diff_image2",back_greyImage);

    absdiff(frame_greyImage ,back_greyImage ,diff_image);
    count_img.create(frame_greyImage.size(),CV_8UC1);
    count_img.setTo(0);

    threshold(diff_image, diff_image, 30 , 255 , CV_THRESH_BINARY );
    erode(diff_image, diff_image, Mat(),Point(-1,-1), 1);
    dilate(diff_image, diff_image, Mat(),Point(-1,-1), 12);

    findContours( diff_image ,contours , hierarchy ,CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Point(0,0));

    vector<vector<Point> >hull( contours.size() );
    objects.clear();
    for( int i = 0; i < contours.size(); i++ )
    {
        if(contourArea(contours[i]) >= detiction_area)
        {
        convexHull( Mat(contours[i]), hull[i], false );
        drawContours( mask_core_frame, hull, (int)i, Scalar( 255), 1, 8, vector<Vec4i>(), 0, Point() );
        Moments mu = moments(hull[i]);
        Point center = Point( mu.m10/mu.m00 , mu.m01/mu.m00 );

        Tracking_Object obj(center ,boundingRect(contours.at(i)),contourArea(hull[i]));
        objects.push_back(obj);
        }
    }


    frame.copyTo(core_frame,mask_core_frame);
    cvtColor(core_frame,core_frame,COLOR_BGR2HSV);
    for(Tracking_Object o : objects)
    {
        circle(mask_core_frame,o.center, 5, Scalar(255,0,0), CV_FILLED, 8);
        //putText(core_frame,o._id,o.center,FONT_HERSHEY_PLAIN,1, Scalar(0,0,255), 1, 2);

    }
    imshow("diff_image4",mask_core_frame);
//    /imshow("diff_image3",core_frame);

/*
    for(unsigned int i=0 ; i< contours.size() ;i++)
    {
        Rect cont_rect = boundingRect(contours.at(i));
        if(cont_rect.area() > 2000 && first_time )
            objects.push_back(cont_rect);
   }

    if( objects.size() > 0)
    {
        if(first_time)
            trackers.add(core_frame,objects);
        else
            trackers.update(core_frame);
        first_time = false;
    }
    else
        first_time = true;

    for(unsigned i=0;i<trackers.objects.size();i++)
    {
        if((trackers.objects[i]).tl().y < 0  ){
            trackers.objects.erase (trackers.objects.begin()+i);
            objects.erase (objects.begin()+i);
            if(i > 0) i--;
        }
        else
           rectangle( core_frame, trackers.objects[i], Scalar( 255, 0, 0 ), 2, 1 );
    }
*/
}

void videoWidget::filter()
{
    cvtColor(frame,filter_out ,CV_RGB2GRAY);
    for(int i=0 ; i < filter_seq.count() ;i++ )
    {
        switch((int)filter_seq.at(i))
        {
        case CANNY_case:
            Canny( filter_out, filter_out, canny_lowthresh, canny_highthresh,3);
            break;
            
        case THRESHOLD_case:
            threshold( filter_out, filter_out , thresh_low , thresh_high , thresh_type );
            break;
            
        case DILATE_case:
            dilate( filter_out, filter_out, 1 );
            break;
            
        case SMOOTH_case:
            //  smooth(filter_out , filter_out ,CV_BLUR_NO_SCALE , 3 );
            break;
        case EQUALIZE_case:
            equalizeHist( filter_out, filter_out );
            break;
        }
    }
    cvtColor(filter_out,frame ,CV_GRAY2RGB);
}

void videoWidget::setSize(int w,int h)
{
    video_height = h;
    video_width = w;
    resize_state = true;
}

void videoWidget::getNewFrame(Mat* newframe)
{
    if(resize_state)
        cv::resize(*newframe , frame  , Size(video_width, video_height)
               ,0,0,INTER_LINEAR);
    cvtColor(frame , frame,CV_BGR2RGB);
    if(firstTime)
    {
        cvtColor(frame , core_frame,CV_BGR2RGB);
        firstTime = false;
    }
    if(filter_state)
        detect();
    this->update();
}

void videoWidget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    QImage tImg((const uchar*)frame.data, frame.cols, frame.rows , QImage::Format_RGB888);
    p.drawImage(0,0,tImg);
}
