#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "video_widget.h"
#include "video_thread.h"
#include "image_thread.h"
#include "image_widget.h"
#include "masksetting.h"

#include <opencv/highgui.h>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/video/tracking.hpp>

#include <QStringListModel>
namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;

public slots:
    void on_openvideobtn_clicked();
    void on_pushButton_background_clicked();
    void on_pushButton_detect_clicked();
    void on_pushButton_set_mask_clicked();
    void on_spinBox_detiction_area_valueChanged(int);
    void setslidervalue(int);

public:
    bool openvideo;
    bool openimage;
    videoWidget *originalVideo;
    imageWidget *originalImage;
    video_thread *my_vid_thread;
    Mat back;

    QMutex mutex;
};

#endif // WIDGET_H
