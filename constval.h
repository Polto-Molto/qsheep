#ifndef CONSTVAL_H
#define CONSTVAL_H

#define CANNY_case          0
#define THRESHOLD_case      1
#define SMOOTH_case         2
#define DILATE_case         3
#define EQUALIZE_case       4

#define TRIANGLE_shape      3
#define RECTANGLE_shape     4
#define PENTAGONAL_shape    5
#define HEXAGONAL_shape     6

#endif // CONSTVAL_H
