#ifndef MASKSETTING_H
#define MASKSETTING_H

#include <QWidget>
#include "polygonmask.h"
#include "image_widget.h"

namespace Ui {
class masksetting;
}

class masksetting : public QWidget
{
    Q_OBJECT

public:
    explicit masksetting(Mat,QWidget *parent = 0);
    ~masksetting();

signals:
    void newMaskCreated();

public slots:
    void on_pushButton_undo_clicked();
    void on_pushButton_clear_clicked();
    void on_pushButton_save_clicked();
private:
    Ui::masksetting *ui;
    polygonmask *mask;
    Mat frame;
};

#endif // MASKSETTING_H
