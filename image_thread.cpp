#include "image_thread.h"
#include <QDebug>
#include <opencv/cv.h>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/flann/flann.hpp>

using namespace cv;

image_thread::image_thread(QObject *parent) :
    QThread(parent)
{
    fini = true;
}

image_thread::~image_thread()
{
    mutex.lock();
    cap.release();
    mutex.unlock();
}

void image_thread::fin()
{
    fini = false;
}

void image_thread::setVideoFile(QString fn)
{
    mutex.lock();
    capframe = imread(fn.toUtf8().data());
    mutex.unlock();
}

void image_thread::run()
{
    while(fini)
    {
       mutex.lock();
       Q_EMIT(commingFrame(&capframe));
       msleep(50);
       mutex.unlock();
    }
    capframe.release();
    exit(0);
}
