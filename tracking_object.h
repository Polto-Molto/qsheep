#ifndef TRACKING_OBJECT_H
#define TRACKING_OBJECT_H
#include <iostream>

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

class Tracking_Object
{
public:
    Tracking_Object(Point,Rect,double);
    void update(Point,Rect , double);
    double matchingValue(Tracking_Object);


    string _id;
    Point center;
    double area;
    double shape;
    static int _num;
};

#endif // TRACKING_OBJECT_H
