#-------------------------------------------------
#
# Project created by QtCreator 2016-10-28T04:32:37
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qsheep
TEMPLATE = app

INCLUDEPATH += /usr/include/

unix:!macx {
    INCLUDEPATH += /usr/local/include/opencv2
    LIBS += -L/usr/local/lib

 LIBS += /usr/local/lib/libopencv_bgsegm.so
 LIBS += /usr/local/lib/libopencv_plot.so
 LIBS += /usr/local/lib/libopencv_bioinspired.so
 LIBS += /usr/local/lib/libopencv_reg.so
 LIBS += /usr/local/lib/libopencv_calib3d.so
 LIBS += /usr/local/lib/libopencv_rgbd.so
 LIBS += /usr/local/lib/libopencv_ccalib.so
 LIBS += /usr/local/lib/libopencv_saliency.so
 LIBS += /usr/local/lib/libopencv_core.so
 LIBS += /usr/local/lib/libopencv_shape.so
 LIBS += /usr/local/lib/libopencv_datasets.so
 LIBS += /usr/local/lib/libopencv_stereo.so
 LIBS += /usr/local/lib/libopencv_dnn.so
 LIBS += /usr/local/lib/libopencv_stitching.so
 LIBS += /usr/local/lib/libopencv_dpm.so
 LIBS += /usr/local/lib/libopencv_structured_light.so
 LIBS += /usr/local/lib/libopencv_face.so
 LIBS += /usr/local/lib/libopencv_superres.so
 LIBS += /usr/local/lib/libopencv_features2d.so
 LIBS += /usr/local/lib/libopencv_surface_matching.so
 LIBS += /usr/local/lib/libopencv_flann.so
 LIBS += /usr/local/lib/libopencv_text.so
 LIBS += /usr/local/lib/libopencv_fuzzy.so
 LIBS += /usr/local/lib/libopencv_tracking.so
 LIBS += /usr/local/lib/libopencv_highgui.so
 LIBS += /usr/local/lib/libopencv_ts.a
 LIBS += /usr/local/lib/libopencv_imgcodecs.so
 LIBS += /usr/local/lib/libopencv_videoio.so
 LIBS += /usr/local/lib/libopencv_imgproc.so
 LIBS += /usr/local/lib/libopencv_video.so
 LIBS += /usr/local/lib/libopencv_line_descriptor.so
 LIBS += /usr/local/lib/libopencv_videostab.so
 LIBS += /usr/local/lib/libopencv_ml.so
 LIBS += /usr/local/lib/libopencv_xfeatures2d.so
 LIBS += /usr/local/lib/libopencv_objdetect.so
 LIBS += /usr/local/lib/libopencv_ximgproc.so
 LIBS += /usr/local/lib/libopencv_optflow.so
 LIBS += /usr/local/lib/libopencv_xobjdetect.so
 LIBS += /usr/local/lib/libopencv_phase_unwrapping.so
 LIBS += /usr/local/lib/libopencv_xphoto.so
 LIBS += /usr/local/lib/libopencv_photo.so

}


macx {
INCLUDEPATH += /usr/local/include/
INCLUDEPATH += /usr/local/include/opencv2/

LIBS += -L/usr/local/lib

LIBS += /usr/local/lib/libopencv_calib3d.dylib
LIBS += /usr/local/lib/libopencv_core.dylib
LIBS += /usr/local/lib/libopencv_features2d.dylib
LIBS += /usr/local/lib/libopencv_flann.dylib
LIBS += /usr/local/lib/libopencv_highgui.dylib
LIBS += /usr/local/lib/libopencv_imgcodecs.dylib
LIBS += /usr/local/lib/libopencv_imgproc.dylib
LIBS += /usr/local/lib/libopencv_ml.dylib
LIBS += /usr/local/lib/libopencv_objdetect.dylib
LIBS += /usr/local/lib/libopencv_photo.dylib
LIBS += /usr/local/lib/libopencv_shape.dylib
LIBS += /usr/local/lib/libopencv_stitching.dylib
LIBS += /usr/local/lib/libopencv_superres.dylib
LIBS += /usr/local/lib/libopencv_ts.a
LIBS += /usr/local/lib/libopencv_video.dylib
LIBS += /usr/local/lib/libopencv_videoio.dylib
LIBS += /usr/local/lib/libopencv_videostab.dylib
}

SOURCES += main.cpp\
        widget.cpp \
    video_thread.cpp \
    video_widget.cpp \
    image_thread.cpp \
    image_widget.cpp \
    polygonmask.cpp \
    masksetting.cpp \
    tracking_object.cpp

HEADERS  += widget.h \
    video_thread.h \
    constval.h \
    video_widget.h \
    image_thread.h \
    image_widget.h \
    polygonmask.h \
    masksetting.h \
    tracking_object.h

FORMS    += widget.ui \
    masksetting.ui
