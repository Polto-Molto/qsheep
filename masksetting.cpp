#include "masksetting.h"
#include "ui_masksetting.h"

masksetting::masksetting(Mat m ,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::masksetting)
{
    ui->setupUi(this);
    mask = new polygonmask(m,this);
    mask->setFixedSize(m.cols, m.rows);
    ui->verticalLayout->insertWidget(0,mask);
}

masksetting::~masksetting()
{
    delete ui;
}

void masksetting::on_pushButton_undo_clicked()
{
    mask->undo();
}

void masksetting::on_pushButton_clear_clicked()
{
    mask->clear();
}

void masksetting::on_pushButton_save_clicked()
{
    mask->renderToImage();
    Q_EMIT (newMaskCreated());
}
