#include "image_widget.h"
#include <QPainter>

imageWidget::imageWidget(QWidget *parent) : QWidget(parent)
{
    filter_state = false;
    resize_state = false;
    video_height = 0;
    video_width = 0;
}

void imageWidget::initwidget(image_thread *vid)
{
   connect(vid , SIGNAL(commingFrame(Mat*)) , this ,SLOT(getNewFrame(Mat*)));
}

void imageWidget::filter()
{
    cvtColor(frame,filter_out ,CV_RGB2GRAY);
    for(int i=0 ; i < filter_seq.count() ;i++ )
    {
        switch((int)filter_seq.at(i))
        {
        case CANNY_case:
                    Canny( filter_out, filter_out, canny_lowthresh, canny_highthresh,3);
                break;

        case THRESHOLD_case:
                    threshold( filter_out, filter_out , thresh_low , thresh_high , thresh_type );
                break;

        case DILATE_case:
                    dilate( filter_out, filter_out, 1 );
                break;

        case SMOOTH_case:
                  //  smooth(filter_out , filter_out ,CV_BLUR_NO_SCALE , 3 );
                break;
        case EQUALIZE_case:
                    equalizeHist( filter_out, filter_out );
                break;
        }
    }
    cvtColor(filter_out,frame ,CV_GRAY2RGB);
}

void imageWidget::setSize(int w,int h)
{
    video_height = h;
    video_width = w;
    resize_state = true;
}

void imageWidget::getNewFrame(Mat* newframe)
{
    if(resize_state)
    {
        cv::resize(*newframe , frame  , Size(video_width, video_height)
               ,0,0,INTER_LINEAR);
        cvtColor(frame , frame,CV_BGR2RGB);
    }
    else
        cvtColor(*newframe , frame,CV_BGR2RGB);
    if(filter_state)
        filter();
    this->update();
}

void imageWidget::paintEvent(QPaintEvent *event)
{
    QPainter p(this);
    QImage tImg((const uchar*)frame.data, frame.cols, frame.rows , QImage::Format_RGB888);
    p.drawImage(0,0,tImg);
}
