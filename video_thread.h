#ifndef VIDEO_THREAD_H
#define VIDEO_THREAD_H

#include <QThread>
#include <QMutex>
#include <opencv/cv.h>
#include <opencv/highgui.h>
//#ifdef __APPLE__
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//#endif
using namespace cv;

class video_thread : public QThread
{
    Q_OBJECT
public:
    explicit video_thread(QObject *parent = 0);
    ~video_thread();

    QMutex mutex;
    void setVideoFile(QString);
    void run();


signals:
    void commingFrame(Mat* );
    void frameNum(int);
    void stopped();

public slots:
    bool openFile(QString);
    void setFinished();
    void goto_frame(int);
    int getFramesNum();
    int getCurrentFramesNum();
    void stop();
    void play();
    int backward();
    int forward();

private:
    VideoCapture capture;
    int frameCount;
    int frameRate;
    Mat frame;
    bool playFlag;
    bool finish;
    int RateAcc;

};

#endif // VIDEO_THREAD_H
