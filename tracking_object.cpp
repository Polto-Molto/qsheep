#include "tracking_object.h"

int Tracking_Object::_num = 0;

Tracking_Object::Tracking_Object(Point p,Rect r,double a)
{
    _num++;
    _id = std::to_string(_num);
    center = p;
    area = a;
    shape = r.x/r.y;
}

void Tracking_Object::update(Point p,Rect r,double a)
{
    center = p;
    area = a;
    shape = r.x/r.y;
}

double Tracking_Object::matchingValue(Tracking_Object o)
{
    double xDiff = this->center.x - o.center.x;
    double yDiff = this->center.y - o.center.y;
    double dist = sqrt((xDiff * xDiff) + (yDiff * yDiff));
    double shape_diff = (this->shape / o.shape)/o.area;
    return dist + shape_diff;
}
