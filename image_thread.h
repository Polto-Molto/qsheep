#ifndef IMAGE_THREAD_H
#define IMAGE_THREAD_H

#include <QThread>
#include <QMutex>

#include <opencv/cv.h>
#include <opencv/highgui.h>

//#ifdef __APPLE__
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
//#endif
using namespace cv;

class image_thread : public QThread
{
    Q_OBJECT
public:
    explicit image_thread(QObject *parent = 0);
    ~image_thread();

    QMutex mutex;
    void setVideoFile(QString);
    void run();

public Q_SLOTS:
    void fin();

signals:
    void commingFrame(Mat* );


public:
    VideoCapture cap;
    int fps;
    bool fini;
    Mat capframe;

};

#endif // VIDEO_THREAD_H
