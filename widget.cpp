#include "widget.h"
#include "ui_widget.h"
#include <QFileDialog>
#include <QDebug>
#include <stdio.h>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    originalVideo  = new videoWidget(this);
    originalImage  = new imageWidget(this);

    originalVideo->setSize(320,240);
    originalImage->setSize(320,240);

    openvideo = false;
    openimage = false;

    my_vid_thread = 0;
    connect(ui->horizontalSlider,SIGNAL(valueChanged(int))
            ,this,SLOT(setslidervalue(int)));
}

Widget::~Widget()
{
  //  my_vid_thread->fin();
  //  my_vid_thread->quit();
    delete ui;
}

void Widget::setslidervalue(int i)
{
    my_vid_thread->goto_frame(i);
}


void Widget::on_openvideobtn_clicked()
{
    openimage = false;
    openvideo = true;

    QString  filename = QFileDialog::getOpenFileName(this,
           tr("Open Video File"), "/home/", tr("Code Files (*.avi *.mpg *.mp4 *.mov)"));
   if(filename != NULL)
   {
    openvideo = true;
    my_vid_thread = new video_thread(this);

    my_vid_thread->openFile(filename);
    originalVideo->initwidget(my_vid_thread);
    ui->horizontalSlider->setMaximum(my_vid_thread->getFramesNum());
    my_vid_thread->start();
    ui->verticalLayout->insertWidget(3,originalVideo);
   }
}

void Widget::on_pushButton_background_clicked()
{
    back = originalVideo->getCurrentFrame().clone();
    originalImage->getNewFrame(&back);
    ui->verticalLayout->insertWidget(4,originalImage);
    originalVideo->setBackGround(back);
}

void Widget::on_pushButton_detect_clicked()
{
    originalVideo->filter_state = true;
}

void Widget::on_pushButton_set_mask_clicked()
{
    masksetting *mask = new masksetting(back);
    connect(mask,SIGNAL(newMaskCreated()),originalVideo,SLOT(setMask()));
    mask->show();
}

void Widget::on_spinBox_detiction_area_valueChanged(int d)
{
    originalVideo->detiction_area = d;
}
