#ifndef IMAGEWIDGET_H
#define IMAGEWIDGET_H

#include <QWidget>
#include "image_thread.h"
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <constval.h>
#ifdef __APPLE__
#include <opencv2/imgproc/imgproc.hpp>
#endif
using namespace cv;

class imageWidget : public QWidget
{
    Q_OBJECT
public:
    explicit imageWidget(QWidget *parent = 0);
    Mat frame;
    void initwidget(image_thread *);
    void filter();
    void setSize(int,int);

signals:

public slots:
    void getNewFrame(Mat*);

protected:
    void paintEvent(QPaintEvent *);

public:
    bool resize_state;
    int video_width,video_height;

    QList<int> filter_seq;
    bool filter_state;
    Mat filter_out;
    int canny_lowthresh,canny_highthresh,thresh_type;
    int thresh_low,thresh_high;
    int dilate_val , erode_val;

    double alfa;
};

#endif // OUT_WIDGET_H
