#include "video_thread.h"
#include <QDebug>
#include <opencv/cv.h>
#ifdef __APPLE__
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/flann/flann.hpp>
#endif

using namespace cv;

video_thread::video_thread(QObject *parent) :
    QThread(parent)
{
    frameRate = 30;
    frameCount = 0;
    finish = false;
    playFlag = false;
    RateAcc = frameRate;
}

video_thread::~video_thread()
{

}

void video_thread::stop()
{
    playFlag = false;
}

void video_thread::play()
{
    playFlag = true;
}

int video_thread::backward()
{
    RateAcc = RateAcc /2;
    if(RateAcc < 4)
        RateAcc = 4;
    if(RateAcc == 15)
        RateAcc ++;
    return RateAcc;
}

int  video_thread::forward()
{
    RateAcc = RateAcc * 2;
    if(RateAcc  > 240)
        RateAcc = 240;
    if(RateAcc == 32)
        RateAcc = 30;
    return RateAcc;
}

void video_thread::goto_frame(int frame_pos)
{
    mutex.lock();
    if(frame_pos < 0 || frame_pos > frameCount )
        frame_pos = 0;
    capture.set(CAP_PROP_POS_FRAMES, frame_pos);
    mutex.unlock();
}

bool video_thread::openFile(QString file){
    bool state = false;
    bool testOpen = this->capture.open(file.toUtf8().data());
    if(testOpen) {
        frameCount = getFramesNum();
        if( frameCount > 0) {
            finish = true;
            play();
            state = true;
        }
    }
    return state;
}

int video_thread::getCurrentFramesNum()
{
    return (int)capture.get(CAP_PROP_POS_FRAMES);
}

int video_thread::getFramesNum()
{
    return (int)capture.get(CAP_PROP_FRAME_COUNT)-1;
}

void video_thread::setFinished()
{
    finish = false;
}

void video_thread::run()
{
    while(finish)
    {
        if(playFlag){
            if(getCurrentFramesNum() >= frameCount)
                capture.set(CAP_PROP_POS_FRAMES, 0);
            mutex.lock();
            this->capture >> frame;
            mutex.unlock();

            if( !frame.empty()) {
                Q_EMIT(commingFrame(&frame));

                //Q_EMIT(frameNum(getCurrentFramesNum()));
            }

            this->msleep((unsigned long)(3000.0 / RateAcc));
        }
    }
    this->capture.release();
    Q_EMIT(stopped());
}
